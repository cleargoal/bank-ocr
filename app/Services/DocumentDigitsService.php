<?php

namespace App\Services;

class DocumentDigitsService
{

    /**
     * Digits received from the OCR document
     *
     * @var array
     */
    protected $docDigits = [];

}

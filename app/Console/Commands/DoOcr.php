<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Command\Command as CommandAlias;

class DoOcr extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'do:ocr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command does OCR document parsing';

    /**
     * Sample digits array
     *
     * @var array
     */
    protected array $sampleDigits = [];

    /**
     * Source digits map
     *
     * @var array
     */
    protected array $srcMap = [];

    /**
     * Mode. One of config('digits.modes')
     *
     * @var string
     */
    protected string $mode = '';

    /**
     * Lines map:
     * 'up' - upper
     * 'mi' - middle
     * 'bt' - bottom
     *
     * @var array
     */
    protected array $linesMap = [];

    /**
     * Result accounts list
     *
     * @var array
     */
    protected array $accounts = [];

    /**
     * Sample digits as string in array
     *
     * @var array
     */
    protected array $sampleDigitsArr = [];

    /**
     * Flags for result table
     *
     * @var array
     */
    protected array $flags = [];

    /**
     * Counter for symbols rows of 1 digits row
     *
     * @var int
     */
    protected int $digitsRowCounter = 0;

    /**
     * Marker of illegible for 1 account
     *
     * @var bool
     */
    protected bool $markIllegible = false;

    /**
     * Current account number in the parse and decode cycle
     *
     * @var string
     */
    protected string $currentAccNumber = '';

    /**
     * Array of randomly generated quasi OCR accounts - for show them
     *
     * @var array
     */
    protected array $randomOcr = [];

    /**
     * Array of randomly generated quasi OCR accounts - for decode them
     *
     * @var array
     */
    protected array $randomOcrArray = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->sampleDigits = config('digits.digits');
        $this->srcMap = config('digits.srcMap');
        $this->linesMap = config('digits.lines');
        $this->flags = config('digits.flags');
        $this->makeSampleDigitsArr();
        $this->setMode();
        $this->runMode();
        if ($this->mode !== 's') {
            $this->printResult();
        }
        return CommandAlias::SUCCESS;
    }

    /**
     * Make Source Digits Array of account
     *
     * @return bool
     */
    protected function makeSampleDigitsArr(): bool
    {
        foreach ($this->sampleDigits as $sampleDigit) {
            $this->sampleDigitsArr[] = implode(Arr::collapse($sampleDigit));
        }
        return true;
    }

    /**
     * Choose the mode
     *
     * @return bool
     */
    protected function setMode(): bool
    {
        $this->newLine();
        $this->error('Set mode');
        $this->question('Please, choose the mode from the list below.');
        $modes = config('digits.modes');
        foreach ($modes as $key => $mode) {
            $this->info($key . ' => ' . $mode);
        }
        $this->mode = $this->ask('Press the key letter you see at the start of line and then press ENTER.');
        $this->comment('You chosen mode: ' . config('digits.modes.' . $this->mode));
        $this->newLine();
        return true;
    }

    /**
     * Run the mode chosen
     *
     * @return bool
     */
    protected function runMode(): bool
    {
        switch ($this->mode) {
            case 's':
                $this->showSampleDigits();
                break;
            case 'e':
                $this->checkTestCases();
                break;
            case 'r':
                $this->checkRandomCases();
                break;
        }
        return true;
    }

    /**
     * Print result
     *
     * return bool
     */
    protected function printResult(): bool
    {
        $this->table(['No', 'Acc Number', 'Flag', 'AMB numbers',], $this->accounts);
        return true;
    }

    /**
     * Show sample digits
     *
     * @return bool
     */
    protected function showSampleDigits(): bool
    {
        $this->error(config('digits.modes.' . $this->mode));
        foreach ($this->sampleDigits as $key => $digit) {
            foreach ($digit as $line => $item) {
                $digitLine = '';
                foreach ($item as $k => $row) {
                    $digitLine .= $row === 1 ? $this->srcMap[$k] : ' ';
                }
                $this->info($digitLine);
            }
        }

        $this->newLine();
        return true;
    }

    /**
     * Check test cases from the task description
     *
     * @return bool
     */
    protected function checkTestCases(): bool
    {
        $this->newLine();
        $this->error(config('digits.modes.' . $this->mode));
        $exampleDigits = file(resource_path('source\example.txt'), FILE_IGNORE_NEW_LINES);
        $this->parseLines($exampleDigits);
        return true;
    }

    /**
     * Create random 1000 records and check them
     *
     * @return bool
     */
    protected function checkRandomCases(): bool
    {
        $this->newLine();
        $this->error(config('digits.modes.' . $this->mode));
        $numbers = [1, 2, 5, 20, 50, 100, 200, 500,];
        $this->newLine();
        $this->question('Please, choose the  number of "accounts", encoded by OCR, those you want to see from the list below.');

        foreach ($numbers as $i => $number) {
            $this->info($i . ': ' . $number);
        }

        $chosenNum = $this->ask('Press the digit key you see at the start of line and then press ENTER');

        $this->generateRandomData($numbers[$chosenNum]);

        $this->newLine();
        $this->info('Random Symbols: ');
        $this->goThroughList($this->randomOcr); // show random OCR symbols

        foreach ($this->randomOcrArray as $item) {
            $this->digitsRowCounter++;
            $singleAccNumber = $this->decodeDigitsRow($item, true);
            $this->accounts[] = $singleAccNumber;
        }

        return true;
    }

    /**
     * Parse line by line to convert symbols '_' and '|' to 0 or 1
     *
     * @param array
     * @return bool
     */
    protected function parseLines($allDigits): bool
    {
        $encodedDigits = [];
        // lines counter to know which line is being processed in a given iteration: up, mi, bt, em(pty)
        $linesCounter = 0;
        foreach ($allDigits as $digitLine) {
            if ($linesCounter < 3) {
                $splatted = str_split($digitLine, 3);
                foreach ($splatted as $splatKey => $splat) {
                    // split the string into 3 characters and parse it into 0 and 1, then save it to an array as an array element
                    $indexing = str_replace([' ', '_', '|'], [0, 1, 1], $splat);
                    $element = str_split($indexing);
                    $encodedDigits[$splatKey][$this->linesMap[$linesCounter]] = $element;
                }
                $linesCounter++;
            } else {
                $this->digitsRowCounter++;
                $singleAccNumber = $this->decodeDigitsRow($encodedDigits,  true);
                $this->accounts[] = $singleAccNumber;
                $linesCounter = 0;
            }
        }
        return true;
    }

    /**
     * Decode 1 account
     *
     * @param array $encodedDigits
     * @param bool $guessing
     * @return array|bool
     */
    protected function decodeDigitsRow(array $encodedDigits, bool $guessing = false): array|bool
    {
        $this->currentAccNumber = '';
        $this->markIllegible = false;
        foreach ($encodedDigits as $digit) {
            $stringDigit = implode(Arr::collapse($digit));
            $illegible = false;
            foreach ($this->sampleDigitsArr as $sampleKey => $stringSample) {

                if ($stringDigit === $stringSample) {
                    $this->currentAccNumber .= $sampleKey;
                    $illegible = false;
                    break;
                } else {
                    $illegible = true;
                }
            }
            if ($illegible) {
                $this->currentAccNumber .= '?';
                $this->markIllegible = true;
            }
        }
        $output = [
            'no' => $this->digitsRowCounter,
            'number' => $this->currentAccNumber,
            'flag' => '',
            'amb' => '',
        ];

        // transforms output; not used when try guessing
        if ($guessing) {
            $output = $this->guessing($encodedDigits, $output);
        }
        // no guessing because method called from guessing section
        if (!$guessing && $this->markIllegible) {
            return !$this->markIllegible;
        }
        else {
            // 1 row of final accounts array
            return $output;
        }
    }

    /**
     * Validate account number.
     * Return 'true' when Account Number valid and 'false' - when error
     *
     * @return bool
     */
    protected function validateAccNumber(): bool
    {
        $positioned = array_combine(config('digits.positions'), str_split($this->currentAccNumber));
        $calculated = [];
        $calc = config('digits.calc');
        foreach ($positioned as $key => $item) {
            $calculated[] = intval($item) + $calc[$key];
        }
        return array_product($calculated) % 11 === 0;
    }

    /**
     * Guessing dispatcher.
     *
     * @param $encodedDigits
     * @param $output
     * @return mixed
     */
    protected function guessing($encodedDigits, $output)
    {
        $flag = 'cor';

        if ($this->markIllegible) {
            $flag = 'ill';
        }
        elseif (!$this->validateAccNumber()) {
            $flag = 'err';
        }

        $correct = $this->tryGuessCorrect($encodedDigits, $flag);

        if ($correct['correct'] === true) {
            // change account number on found correct
            $output['number'] = $correct['value'];
        }
        $output['flag'] = $correct['flag'];
        $output['amb'] = $correct['amb'];

        return $output;
    }

    /**
     * Try guess correct digit
     *
     * @param $encodedDigits
     * @param $flag
     * @return array
     */
    protected function tryGuessCorrect($encodedDigits, $flag): array
    {
        $rememberInit = $this->currentAccNumber;
        $foundCorrect = [];
        foreach ($encodedDigits as $cKey => $enDigit) {
            foreach ($enDigit as $dKey => $digLine) {
                foreach ($digLine as $lKey => $segment) {
                    $encodedDigits[$cKey][$dKey][$lKey] = $segment === '0' ? '1' : '0';
                    $decodeAcc = $this->decodeDigitsRow($encodedDigits); // $guessing = false; returns false if Illegible
                    if($decodeAcc && $this->validateAccNumber()) {
                        $foundCorrect[] = $decodeAcc;
                    }
                    $encodedDigits[$cKey][$dKey][$lKey] = $segment; // revert to previous value
                }
            }
        }

        if(count($foundCorrect) === 1) {
            $guess = ['correct' => true, 'value' => $foundCorrect[0]['number'], 'flag' => '', 'amb' => '',];
        } elseif(count($foundCorrect) === 0) {
            $guess = ['correct' => false, 'value' => $rememberInit, 'flag' => $this->flags[$flag], 'amb' => '',];
        }
        else {
            $guess = ['correct' => true, 'value' => $foundCorrect[0]['number'], 'flag' => $this->flags['amb'],
                'amb' => count($foundCorrect),];
        }

        return $guess;
    }

    /**
     * Generate random ocr-like data
     *
     * @param int $number
     * @return bool
     */
    protected function generateRandomData(int $number): bool
    {
        $this->randomOcr = [];
        $this->randomOcrArray = [];
        for ($i = 0; $i < $number; $i++) {
            $randomAcc = $this->generateRandomAcc();
            $randomCorrupt = rand(0, 1);
            for ($k = 0; $k < 4; $k++) {
                for ($j = 0; $j < 9; $j++) {
                    $currantDigit = intval($randomAcc[$j]);
                    for ($t = 0; $t < 3; $t++) {
                        if ($this->linesMap[$k] !== 'em') {
                            // use correct symbol
                            $currentSymbol = $this->sampleDigits[$currantDigit][$this->linesMap[$k]][$t];
                            if ($randomCorrupt === 1) {
                                $doCorruption = rand(0, 1); // do or don't corrupt symbol
                                if ($doCorruption === 1) {
                                    $currentSymbol = $this->sampleDigits[$currantDigit][$this->linesMap[$k]][$t] === 0 ? 1 : 0;
                                    $randomCorrupt = 0;
                                }
                            }
                            $this->randomOcr[$i][$this->linesMap[$k]][$j][$t] = $currentSymbol;
                            $this->randomOcrArray[$i][$j][$this->linesMap[$k]][$t] = $currentSymbol;
                        } else {
                            $this->randomOcr[$i][$this->linesMap[$k]][$j][$t] = '0';
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Generate random Account
     *
     * @return string
     */
    protected function generateRandomAcc(): string
    {
        $randomAcc = '';
        for ($d = 0; $d < 9; $d++) {
            $randomAcc .= (rand(0,9));
        }
        return$randomAcc;
    }

    /**
     * Go through OCR 'accounts' list
     *
     * @param $listAccounts
     * @return bool
     */
    protected function goThroughList($listAccounts): bool
    {
        foreach ($listAccounts as $lKey => $listAcc) {
            $this->showFromRowArray($listAcc);
        }
        return true;
    }

    /**
     * Go through one row digits array and show OCR digits
     *
     * @param array $digitsRow
     * @return void
     */
    protected function showFromRowArray(array $digitsRow)
    {
        foreach ($digitsRow as $key => $digit) {
            $digitLine = '';
            foreach ($digit as $line => $item) {
                foreach ($item as $k => $row) {
                    $digitLine .= $row === 1 ? $this->srcMap[$k] : ' ';
                }
            }
            $this->info($digitLine);
        }
    }

}

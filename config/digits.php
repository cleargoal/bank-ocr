<?php
// every digit described by 3 arrays corresponding 3 line of symbols;
// the 1st line contains only horizontal line or nothing;
// the 2nd line may contain in 0 position vertical line, in 1 position - horizontal line and in 2 position vertical line;
// the 3d line is similar as 2nd
// arrays values are: 0 - no line, 1 - is line

return [
    'digits' =>
        [
            0 => [
                'up' => [0, 1, 0,],
                'mi' => [1, 0, 1,],
                'bt' => [1, 1, 1,],
            ],
            1 => [
                'up' => [0, 0, 0,],
                'mi' => [0, 0, 1,],
                'bt' => [0, 0, 1,],
            ],
            2 => [
                'up' => [0, 1, 0,],
                'mi' => [0, 1, 1,],
                'bt' => [1, 1, 0,],
            ],
            3 => [
                'up' => [0, 1, 0,],
                'mi' => [0, 1, 1,],
                'bt' => [0, 1, 1,],
            ],
            4 => [
                'up' => [0, 0, 0,],
                'mi' => [1, 1, 1,],
                'bt' => [0, 0, 1,],
            ],
            5 => [
                'up' => [0, 1, 0,],
                'mi' => [1, 1, 0,],
                'bt' => [0, 1, 1,],
            ],
            6 => [
                'up' => [0, 1, 0,],
                'mi' => [1, 1, 0,],
                'bt' => [1, 1, 1,],
            ],
            7 => [
                'up' => [0, 1, 0,],
                'mi' => [0, 0, 1,],
                'bt' => [0, 0, 1,],
            ],
            8 => [
                'up' => [0, 1, 0,],
                'mi' => [1, 1, 1,],
                'bt' => [1, 1, 1,],
            ],
            9 => [
                'up' => [0, 1, 0,],
                'mi' => [1, 1, 1,],
                'bt' => [0, 1, 1,],
            ],
        ],
    'srcMap' => ['|', '_', '|'],
    'modes' => [
        's' => 'Show source digits',
        'e' => 'Check Example test cases from the task description',
        'r' => 'Create Random lot of records and check them',
    ],
    'lines' => [0 => 'up', 1 => 'mi', 2 => 'bt', 3 => 'em'],
    'positions' => ['d9', 'd8', 'd7', 'd6', 'd5', 'd4', 'd3', 'd2', 'd1'],
    'calc' => ['d9' => 0, 'd8' => 9, 'd7' => 8, 'd6' => 7, 'd5' => 6, 'd4' => 5, 'd3' => 4, 'd2' => 3, 'd1' => 2],
    'guessing' => [
        0 => [8],
        1 => [7],
        3 => [9],
        5 => [9, 6,],
        6 => [5, 8,],
        7 => [1],
        8 => [0, 9, 6,],
        9 => [8],
    ],
    'flags' => ['ill' => 'ILL', 'err' => 'ERR', 'amb' => 'AMB', 'cor' => '', ],
];
